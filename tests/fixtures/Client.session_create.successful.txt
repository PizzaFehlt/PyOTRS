>>> client.response_type
'AccessToken'

>>> client.response.status_code
200

>>> client.response.json()
{u'AccessToken': u'tMtTFDg1PxCX51dWnjue4W5oQtNsFd0k'}
